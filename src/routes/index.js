import React from 'react';
import { Router, Route, IndexRoute } from 'react-router';

import { history } from '../store';

import Home from './home';

export default () => {
  return (
    <Router history={history}>
      <Route path="/">
        <IndexRoute exact component={Home} />
      </Route>
    </Router>
  );
}


