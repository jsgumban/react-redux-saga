import { connect } from 'react-redux';
import Home from './home';

function mapStateToProps(state) {
  return {
    age: state.count.age,
  };
}

const mapDispatchToProps = dispatch => {
  return {
    onAgeUp: () => dispatch({ type: "AGE_UP", value: 1 }),
    onAgeDown: () => dispatch({ type: "AGE_DOWN", value: 1 })
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);

