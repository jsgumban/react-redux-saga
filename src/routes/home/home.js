import React, { Component } from 'react';
import './home.scss'

class Home extends Component {
  render() {
    console.log('this.props', this.props);
    return (
      <div className="App">
        <div className="Age-label">
          your age: <span>{this.props.age}</span>
        </div>
        <button onClick={this.props.onAgeUp}>Age UP</button>
        <button onClick={this.props.onAgeDown}>Age Down</button>
      </div>
    );
  }
}

export default Home;
