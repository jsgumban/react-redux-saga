import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './internal/serviceWorker';
import { Provider } from 'react-redux';

import Routes from './routes';
import store from './store';

const app = (
   <Provider store={store}>
      <Routes />
   </Provider>
);

ReactDOM.render(
   app,
   document.getElementById('root'),
);

serviceWorker.unregister();


// import App from './App';
//
// import './core.scss';
//
// ReactDOM.render(
//   <React.StrictMode>
//     <App />
//   </React.StrictMode>,
//   document.getElementById('root')
// );

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA

