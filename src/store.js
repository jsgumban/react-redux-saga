import { createStore, applyMiddleware } from 'redux';
import { syncHistoryWithStore } from 'react-router-redux';
import { browserHistory } from 'react-router';

import createSagaMiddleware from 'redux-saga';
import rootSaga from './sagas/index';

import rootReducer from './reducers/index';

// import sagaMonitor from './monitor';
const sagaMiddleware = createSagaMiddleware( );
const store = createStore(rootReducer, applyMiddleware(sagaMiddleware));
sagaMiddleware.run(rootSaga);

export const history = syncHistoryWithStore(browserHistory, store);
export default store;

